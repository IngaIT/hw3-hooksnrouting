import { createBrowserRouter } from "react-router-dom";

import { App } from "../App";
import { Notfound } from "../pages/notfound";
import { Shop } from "../pages/shop";
import { Cart } from "../pages/cart";
import { Favorites } from "../pages/favorites";

export const router = createBrowserRouter([
  {
    element: <App />,
    path: "/",
    errorElement: <Notfound />,
    children: [
      {
        element: <Shop />,
        index: true,
      },
      {
        element: <Cart />,
        path: "cart",
      },
      {
        element: <Favorites />,
        path: "favorites",
      },
    ],
  },
]);
