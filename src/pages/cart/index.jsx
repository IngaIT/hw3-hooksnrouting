import React from "react";
import PropTypes from "prop-types";
import { useOutletContext } from "react-router-dom";
import styles from "../../styles/cart.module.scss";
import { Product } from "../../components/product";

export const Cart = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInCart,
    handleCartClick,
    setModal,
    setModalData,
  } = useOutletContext();

  const productsCart = products.filter((product) =>
    productsInCart.includes(product.id)
  );

  return (
    <div className={styles.Cart}>
      {!productsCart.length ? (
        <h1>Товари відсутні</h1>
      ) : (
        <ul className={styles.CartList}>
          {productsCart.map((product) => (
            <Product
              key={product.id}
              product={product}
              favorites={favorites}
              handleFavoritesClick={handleFavoritesClick}
              productsInCart={productsInCart}
              handleCartClick={handleCartClick}
              setModal={setModal}
              setModalData={setModalData}
            ></Product>
          ))}
        </ul>
      )}
    </div>
  );
};

Cart.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInCart: PropTypes.array.isRequired,
  handleCartClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};
