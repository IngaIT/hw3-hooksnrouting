export const mat = [
  {
    "mat": [
      {
        "id": 1,
        "name": "Коврик для йоги Bodhi Kailash Premium 200 см",
        "price": 2000,
        "imgUrl": "1.jpg",
        "article": "500101",
        "color": "bordo"
      },
      {
        "id": 2,
        "name": "Коврик для йоги Bodhi Kailash Premium 200 см",
        "price": 2000,
        "imgUrl": "2.jpg",
        "article": "500102",
        "color": "olive"
      },
      {
        "id": 3,
        "name": "Коврик для йоги Bodhi Kailash Premium 200 см",
        "price": 2000,
        "imgUrl": "3.jpg",
        "article": "500103",
        "color": "orange"
      },
      {
        "id": 4,
        "name": "Коврик для йоги Bodhi Kailash Premium 200 см",
        "price": 2000,
        "imgUrl": "4.jpg",
        "article": "500104",
        "color": "violet"
      },
      {
        "id": 5,
        "name": "Коврик для йоги Bodhi Kailash Premium 200 см",
        "price": 2000,
        "imgUrl": "5.jpg",
        "article": "500105",
        "color": "grey"
      },
      {
        "id": 6,
        "name": "Чохол для кoвpикa Easy Bag 80 см",
        "price": 700,
        "imgUrl": "6.jpg",
        "article": "700701",
        "color": "silver"
      },
      {
        "id": 7,
        "name": "Чохол для кoвpикa Easy Bag 80 см",
        "price": 700,
        "imgUrl": "7.jpg",
        "article": "700702",
        "color": "rose"
      },
      {
        "id": 8,
        "name": "Чохол для кoвpикa Easy Bag 80 см",
        "price": 700,
        "imgUrl": "8.jpg",
        "article": "700703",
        "color": "violet"
      },
      {
        "id": 11,
        "name": "Чохол для кoвpикa Easy Bag 80 см",
        "price": 700,
        "imgUrl": "9.jpg",
        "article": "700704",
        "color": "blue"
      },
      {
        "id": 12,
        "name": "Чохол для коврика Bodhi Asana 80 Cotton",
        "price": 1000,
        "imgUrl": "10.jpg",
        "article": "700705",
        "color": "grey"
      }
    ]
  }
  ];

