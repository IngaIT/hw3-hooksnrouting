import React from "react";
import PropTypes from "prop-types";
import styles from "../../styles/header.module.scss";
import { HeaderMenu } from "../headerMenu";
import { Logo } from "../logo";
import { FavIco } from "../icons/favIco";
import { CartIco } from "../icons/cartIco";

export const Header = ({ favorites, productsInCart }) => {
  const countFavorites = favorites.length;
  const countProductsInCart = productsInCart.length;

  return (
    <div className={styles.Header__row}>
      <Logo />
      <HeaderMenu />

      <div className={styles.HeaderActions}>
        <div className={styles.HeaderCart}>
          <CartIco width={26} fill={"#1c8646"} />
          <span className={styles.cartValue}>{countProductsInCart}</span>
        </div>
        <div className={styles.HeaderFavorites}>
          <FavIco width={26} fill={"#ffda12"} />
          <span className={styles.favoritesValue}>{countFavorites}</span>
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {
  favorites: PropTypes.array.isRequired,
  productsInCart: PropTypes.array.isRequired,
};
